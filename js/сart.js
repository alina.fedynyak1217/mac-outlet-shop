import {items} from './items.js'
import {searchProduct} from './index.js'

export class Cart {
    constructor(){
        this.itemsCart = []
        this.totalAmount = 0
        this.totalPrice = 0
        this.cartElement = document.querySelector('cart-products')
        this.initCart()
    }

    addToCart(event) {
        let productId = event.currentTarget.getAttribute('data-product-id')
        let changeCount = false

        if (Array.isArray(this.itemsCart)) {
            this.itemsCart = this.itemsCart.map(item => {
                if (item.id === +productId) {
                    ++item.orderCount
                    changeCount = true
                }

                return item
            })
        }

        if (!changeCount) {
            let productCart = searchProduct(items,productId).shift()

            productCart.orderCount = 1
            this.itemsCart.push(productCart)

        }
        localStorage.setItem('cart', JSON.stringify(this.itemsCart))

        this.initCart()
    }

    initCart() {
        this.itemsCart = JSON.parse(localStorage.getItem('cart'))
        let cartProductsBlock = document.getElementById('cart-products')
        cartProductsBlock.innerHTML = ''
        let cartTotalBlock = document.getElementById('cart-products-total')
        cartTotalBlock.innerHTML = ''
        this.totalAmount = 0
        this.totalPrice = 0

        if (Array.isArray(this.itemsCart) && this.itemsCart.length > 0) {
            let cartHtml = ''
            this.itemsCart.forEach((product) => {
                this.totalAmount += product.orderCount
                this.totalPrice += product.orderCount * product.price

                cartHtml += `<div class="block-in-cart">
                                    <img class="img-in-cart" src="./img/` + product.imgUrl + `" alt="img">
                                    <div class="title-price">
                                        <h3>` + product.name + `</h3>
                                        <div>` + product.price + ` $</div>
                                    </div>
                                    <div class="amount-in-cart">
                                        <img data-product-id="` + product.id + `" id="remove-count" class="remove-count ${ product.orderCount > 1 ? 'active' : 'disabled'}" src="./img/icons/arrow_left.svg" alt="arrow">
                                        <div>` + product.orderCount + `</div>
                                        <img data-product-id="` + product.id + `" id="add-count ${ product.orderCount < 4 ? 'active' : 'disabled'}" class="add-count" src="./img/icons/arrow_left.svg" alt="arrow">
                                        <div data-product-id="` + product.id + `" id="delete" class="delete">&times;</div>
                                    </div>
                                    </div>`
            })

            let cartTotalHtml = `<div class="cart-total">
                            <div>
                                <p>Total amount:` + this.totalAmount + `ptc.</p>
                            </div>
                            <div>
                                <p>Total price: ` + this.totalPrice + `$</p>
                            </div>
                        </div>
                        <button class="buy">Buy</button>`

            cartProductsBlock.insertAdjacentHTML('afterbegin', cartHtml)
            cartTotalBlock.insertAdjacentHTML('afterbegin', cartTotalHtml)
        } else {
            this.itemsCart =[]
            let emptyCart = '<div>You have not added an item to your cart yet!</div>'
            cartProductsBlock.insertAdjacentHTML('afterbegin', emptyCart)
        }
        this.initCartEvents()
        this.addProductsCount()
    }

    initCartEvents() {
        let deleteProducts = document.getElementsByClassName('delete')
        for (let i = 0; i < deleteProducts.length; i++) {
            deleteProducts[i].addEventListener('click', this.deleteProduct.bind(this))
        }
        let addCount = document.getElementsByClassName('add-count')
        for (let i = 0; i < addCount.length; i++) {
            addCount[i].addEventListener('click', this.addCountProduct.bind(this))
        }
        let removeCount = document.getElementsByClassName('remove-count')
        for (let i = 0; i < removeCount.length; i++) {
            removeCount[i].addEventListener('click', this.removeCountProduct.bind(this))
        }
    }

    deleteProduct(e) {
        this.itemsCart = JSON.parse(localStorage.getItem('cart'))
        let productId = e.currentTarget.getAttribute('data-product-id')
        this.itemsCart = this.itemsCart.filter(product => product.id !==  +productId)
        localStorage.setItem('cart', JSON.stringify(this.itemsCart))

        this.initCart()
    }

    addCountProduct(e) {
        this.itemsCart = JSON.parse(localStorage.getItem('cart'))
        let productId = e.currentTarget.getAttribute('data-product-id')
        this.itemsCart = this.itemsCart.map(item => {
            if (item.id === +productId && item.orderCount < 4) {
                ++item.orderCount
            }

            return item
        })
        localStorage.setItem('cart', JSON.stringify(this.itemsCart))

        this.initCart()
    }

    removeCountProduct(e) {
        this.itemsCart = JSON.parse(localStorage.getItem('cart'))
        let productId = e.currentTarget.getAttribute('data-product-id')
        console.log(productId)
        this.itemsCart = this.itemsCart.map(item => {
            if (item.id === +productId && item.orderCount > 1) {
                --item.orderCount
            }

            return item
        })
        localStorage.setItem('cart', JSON.stringify(this.itemsCart))

        this.initCart()
    }

    addProductsCount() {
        let productCountBlock = document.getElementsByClassName('product-count')[0]
        productCountBlock.innerHTML = ''
        productCountBlock.insertAdjacentHTML('afterbegin', this.totalAmount)
    }
}



