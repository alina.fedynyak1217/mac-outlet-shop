import {items} from './items.js'
import {Filter} from './filter.js'
import {Cart} from  './сart.js'

export {createProductsToHtml, addModalEventToProducts, searchProduct}

const filter = new Filter()
const cart = new Cart()

// slick slider
$('.main__sliders').slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
});
// slick slider END

// modalCart
let modalCart = document.getElementById("modalCart");
let btnCart = document.getElementById("myBtn");
let btnCartClose = document.getElementsByClassName("cart-close")[0];

btnCart.onclick = function() {
    modalCart.style.display = "block";
}

btnCartClose.onclick = function() {
    modalCart.style.display = "none";
}

window.onclick = function(event) {
    if (event.target === modalCart) {
        modalCart.style.display = "none";
    }
}
//modalCart END

// filter accordion
let acc = document.getElementsByClassName("accordion-block");

for (let i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {

        this.classList.toggle("active");

        let panel = this.nextElementSibling;
        if (panel === null) {
            return
        }
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
// filter accordion END

// init products HTML
const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const checkInStock =(countProduct) => {
    if (countProduct > 0) {
        return 'in-stock'
    } else {
        return  'out-of-stock'
    }
}

const createProductsToHtml = (products) => {
    let productsHtml = ''
    products.forEach((product) => {
        productsHtml += '<div class="product" data-product-id="' + product.id + '">\n' +
                            '<div class="favorite">\n' +
                                '<div class="like_empty like"></div>\n' +
                            '</div>\n' +
                            '<div class="cards-img">\n' +
                                '<img src="./img/'+ product.imgUrl +'" alt="product">\n' +
                            '</div>\n' +
                                '<h3 class="title-cart">'+ product.name +'</h3>\n' +
                            '<div class="cards-stock">\n' +
                                '<div class="cards-stock-img '+ checkInStock(product.orderInfo.inStock) +'"></div>\n' +
                                '<p class="left-in-stock">' + product.orderInfo.inStock + ' left in stock</p>\n' +
                            '</div>\n' +
                                '<p class="price">Prise: '+ product.price + ' $</p>\n' +
                                '<button data-product-id="' + product.id + '" class="add-to-cart ' + checkInStock(product.orderInfo.inStock) + '">Add to cart</button>\n' +
                            '<div class="info">\n' +
                                '<div class="info-like">\n' +
                                    '<img src="./img/icons/like_filled.svg" alt="like">\n' +
                                    '<p>'+ product.orderInfo.reviews + '%' + ' Positiv reviews' +' </p>\n' +
                                '</div>\n' +
                                '<div class="info-orders">\n' +
                                    '<div>'+ getRandomInt(2,1000) +'</div>\n' +
                                    '<div>orders</div>\n' +
                                '</div>\n' +
                            '</div>\n' +
                        '</div>'
    })
    return productsHtml
}
const productsHtml = createProductsToHtml(items)
let productsBlock = document.getElementById('products')
productsBlock.insertAdjacentHTML('afterbegin', productsHtml)
// init products HTML END

// like button
function updateFavorite(event){
    if (event.target.classList.contains('like_empty') === true) {
        event.target.classList.add('like-filled')
        event.target.classList.remove('like_empty')
    }else {
        event.target.classList.add('like_empty')
        event.target.classList.remove('like-filled')
    }
}

let favoriteButtons = document.getElementsByClassName('like')
for (let i = 0; i < favoriteButtons.length; i++) {
    favoriteButtons[i].addEventListener('click', updateFavorite, false);
}
// like button END

// modal product
const createProductModal = (product) => {
    let modalHTML = '<div class="modal-overlay modal-overlay_visible" data-modal-id="'+ product.id +'">\n' +
                        '<div class="modal-table">\n' +
                            '<div class="modal-table-cell">\n' +
                                '<div class="modal">\n' +
                                    '<div class="modal__close"></div>\n' +
                                        '<div class="modal__content">\n' +
                                            '<div class="modal__product">\n' +
                                                '<div class="modal__product__img">\n' +
                                                    '<img src="./img/'+ product.imgUrl +'" alt="card">\n' +
                                                '</div>\n' +
                                                '<div class="modal__product__info">\n' +
                                                    '<h2 class="title-cart">'+ product.name +'</h2>\n' +
                                                        '<div class="info">\n' +
                                                        '<div class="info-like">\n' +
                                                            '<img src="./img/icons/like_filled.svg" alt="like">\n' +
                                                            '<p>'+ product.orderInfo.reviews + '%' + ' Positiv reviews' +'</p>\n' +
                                                       '</div>\n' +
                                                        '<div class="info-orders">\n' +
                                                        '<div>'+ getRandomInt(2,1000) +'</div>\n' +
                                                        '<div>orders</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +
                                                    '<ul class="info__about">\n' +
                                                        '<li>Color: '+ product.color +'</li>\n' +
                                                        '<li>Operating System: '+ product.os +'</li>\n' +
                                                        '<li>Chip: '+ product.chip.name + ' cores:' + product.chip.cores +'</li>\n' +
                                                        '<li>Height: '+ product.size.height +' cm</li>\n' +
                                                        '<li>Width: '+ product.size.width +' cm</li>\n' +
                                                        '<li>Weight: '+ product.size.weight +' g</li>\n' +
                                                '</ul>\n' +
                                                '</div>\n' +
                                                '<div class="modal__product__price">\n' +
                                                '<p class="price">Prise: '+ product.price + ' $</p>\n' +
                                                    '<button data-product-id="' + product.id + '" class="add-to-cart ' + checkInStock(product.orderInfo.inStock) + '">Add to cart</button>\n' +
                                                    '<p class="left-in-stock">' + product.orderInfo.inStock + ' left in stock</p>\n' +
                                                '</div>\n' +
                                            '</div>\n' +
                                        '</div>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                            '</div>\n' +
                        '</div>'
    return modalHTML
}

const searchProduct = (products, id) => {
    return  products.filter(product => product.id === +id)
}

const showProductModal = (event) => {
    if (!event.target.closest('.favorite') && !event.target.closest('.add-to-cart')) {
        let productId = event.currentTarget.getAttribute('data-product-id')
        let productObj = searchProduct(items,productId).shift()
        let modalHTML = createProductModal(productObj)

        let modalBlock = document.getElementById('modal')
        modalBlock.insertAdjacentHTML('afterbegin', modalHTML)
        modalBlock.getElementsByClassName('add-to-cart')[0].addEventListener('click', cart.addToCart.bind(cart))

        let closeModalButton = document.getElementsByClassName('modal__close')
        closeModalButton[0].addEventListener('click', removeProductModal, false)
        document.addEventListener('click', closeProductModal, false)
    }

}

const closeProductModal = (e) => {
    if (!e.target.closest('.modal') && !e.target.closest('.product')) {
        removeProductModal()
    }
}

const removeProductModal = () => document.getElementById('modal').innerHTML = ""

document.addEventListener("DOMContentLoaded", function(event) {
    addModalEventToProducts()
    addSlideProducts()
    let priceFrom = document.getElementById('price-from')
    priceFrom.addEventListener('keyup', filter.filterSearch)
    let priceTo = document.getElementById('price-to')
    priceTo.addEventListener('keyup', filter.filterSearch)

});
// modal product END

const addModalEventToProducts = () => {
    let products = document.getElementsByClassName('product')

    for (let i = 0; i < products.length; i++) {
        products[i].addEventListener('click', showProductModal, false);
        products[i].querySelector('.add-to-cart').addEventListener('click', cart.addToCart.bind(cart))
    }
}

const addSlideProducts = () => {
    let sliderProducts = document.getElementsByClassName('button-holder')

    for (let i = 0; i < sliderProducts.length; i++) {
        sliderProducts[i].addEventListener('click', cart.addToCart.bind(cart))
    }
}



