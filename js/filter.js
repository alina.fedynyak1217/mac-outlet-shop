import {items} from './items.js'
import {createProductsToHtml, addModalEventToProducts} from './index.js'

export class Filter {

    constructor() {
        this.options = [
            {
                title: 'Price',
                options: this.getMinMaxPrice(),
            },
            {
                title: 'Color',
                options: this.getColor(),
            },
            {
                title: 'Memory',
                options: this.getRam(),
            },
            {
                title: 'OS',
                options: this.getOs(),
            },
            {
                title: 'Display',
                options: this.getDisplay(),
            },
        ]
        this.createFilterHtml()
    }

    getMinMaxPrice(){
    let dataPrice = items.map(item => item.price)
        .sort(function(a, b) {
        return a - b;
    }).filter((item, index, array) =>{
            if (index == 0 || array.length - 1 == index){
                return true
            }
        })
        return dataPrice

    }

    getColor() {
        return items.reduce((acc, item) => {
            item.color.forEach(colorInArr => {
                if (!acc.includes(colorInArr)) {
                    acc.push(colorInArr)
                }
                return
            })
            return acc
        }, [])
    }

    getRam() {
        return items.reduce((acc, item) => {
            if (item.memory !== null && !acc.includes(item.memory)) {
                acc.push(item.memory)
            }
            return acc
        }, [])
    }

    getOs() {
        return items.reduce((acc, item) => {
            if (item.os !== null && !acc.includes(item.os)) {
                acc.push(item.os)
            }
            return acc
        }, [])
    }

    getDisplay() {
        return items.reduce((acc,item) => {
            if (item.display !== null && !acc.includes(item.display)) {
                acc.push(item.display)
            }
            return acc
        }, [])
    }

    createFilterHtml() {
        const filterContainer = document.querySelector('.menu__filter')
        this.options.forEach(item => {
            if (item.title === 'Price') {
                let bodyPrice = document.createElement('div')
                bodyPrice.className = 'panel'
                bodyPrice.innerHTML = `<div class="panel__price">
                            <label for="price-from">From</label>
                            <input type="text" placeholder="${item.options[0]}" id="price-from" name="priceFrom" required minlength="3" maxlength="6" size="10">
                            <label for="price-to">To</label>
                            <input class="input-to-prise" placeholder="${item.options[1]}" id="price-to" type="text" name="priceTo" required minlength="3"
                                   maxlength="6" size="10">
                        </div>`
                filterContainer.appendChild(this.createOptionHeader(item.title))
                filterContainer.appendChild(bodyPrice)
            } else {
                filterContainer.appendChild(this.createOptionHeader(item.title))
                filterContainer.appendChild(this.createOptionBody(item))
            }
        })
    }

    createOptionBody(item) {
        let bodyOption = document.createElement('div')
        bodyOption.className = 'panel'
        item.options.forEach(option => {
            bodyOption.appendChild(this.createHtmlCheckbox(option, item.title))
        })

        return bodyOption
    }

    filterSearch(e) {
        let filterOptions = {}
        let blockOptions =  document.querySelectorAll('.panel')
        blockOptions.forEach((item, key) => {
            if (key === 0) {
                filterOptions.Price = {}
                let elementPriceFrom = document.getElementById('price-from')
                let elementPriceTo = document.getElementById('price-to')

                if (elementPriceFrom.value.length !== 0 && elementPriceFrom.value.length !== 0) {
                    filterOptions.Price.from = elementPriceFrom.value
                } else {
                    filterOptions.Price.from = elementPriceFrom.getAttribute('placeholder')
                }
                if (elementPriceFrom.value.length !== 0 && elementPriceTo.value.length !== 0) {
                    filterOptions.Price.to = elementPriceTo.value
                } else {
                    filterOptions.Price.to = elementPriceTo.getAttribute('placeholder')
                }
            }
            if (key !== 0) {
                let itemOptions = item.querySelectorAll('.option')
                itemOptions.forEach(itemOption => {
                    if (itemOption.checked) {
                        let filterName =  itemOption.getAttribute('name')
                        let filterValue = itemOption.getAttribute('value')
                        if (!filterOptions.hasOwnProperty(filterName)) {
                            filterOptions[filterName] = [filterValue]
                        }
                        if (!filterOptions[filterName].includes(filterValue)) {
                            filterOptions[filterName].push(filterValue)
                        }
                    }
                })
            }
        })

        let filterItems = []
        for (let option in filterOptions) {
                filterItems = items.filter(item => {
                    if (item.price >= filterOptions.Price.from && item.price <= filterOptions.Price.to) {
                        let itemOptions = item[option.toLowerCase()]
                        let find = false
                        if (option !== 'Price') {
                            if (Array.isArray(itemOptions)) {
                                filterOptions[option].forEach(itemFilter => {
                                    if (itemOptions.includes(itemFilter)) {
                                        find = true
                                    }
                                })
                            } else {
                                filterOptions[option].forEach(itemFilter => {
                                    if (itemOptions == itemFilter) {
                                        find = true
                                    }
                                })
                            }
                        } else {
                            find = true
                        }
                        return find
                    }
                })
        }

        let productsBlock = document.getElementById('products')
        productsBlock.innerHTML = ''
        let productsHtml = '<div class="no-found-products">No products found</div>'
        if (filterItems.length !== 0) {
            productsHtml = createProductsToHtml(filterItems)
        }
        productsBlock.insertAdjacentHTML('afterbegin', productsHtml)
        addModalEventToProducts()
    }

    createHtmlCheckbox(option, title) {
        let block = document.createElement('div')
        block.className = 'filter-option'

        let inputCheckbox = document.createElement('input')
        inputCheckbox.type = 'checkbox'
        inputCheckbox.className = 'option'
        inputCheckbox.name = title
        inputCheckbox.value = option
        inputCheckbox.id = option
        inputCheckbox.addEventListener('click', this.filterSearch)

        let label = document.createElement('label')
        label.setAttribute('for', option)
        label.innerHTML = option + this.getParamsName(title)
        block.appendChild(inputCheckbox)
        block.appendChild(label)

        return block
    }

    getParamsName(title){
        if (title === 'Display') {
            return ' inch'
        }
        if (title === 'Memory') {
            return ' Gb'
        }
        return ''
    }

    createOptionHeader(title) {
        let headerOption = document.createElement('div')
        headerOption.className = 'accordion-block'
        headerOption.innerHTML = this.getOptionHtmlHeader(title)

        return headerOption
    }

    getOptionHtmlHeader(title) {
        return `<div class="accordion-block">
                        <img class="accordion-img" src="./img/icons/arrow_left.svg" alt="arrow">
                        <button class="accordion-filter">${title}</button>
                    </div>`
    }
}

